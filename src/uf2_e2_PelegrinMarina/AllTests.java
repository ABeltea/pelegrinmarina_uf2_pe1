package uf2_e2_PelegrinMarina;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ PrimordialTest.class })
public class AllTests {

}
