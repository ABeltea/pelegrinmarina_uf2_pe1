package uf2_e2_PelegrinMarina;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.Assert;
import org.junit.Before;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith (value=Parameterized.class)
public class PrimordialTest {

	private double expected, actual, delta;
	private Primordial primordial = null;
	
	@Parameters
	public static Collection<Double []> testParam() {
		return Arrays.asList(new Double [][] {
			{2.0, 2.0, 0.03},
			{6.0, 3.0, 0.03},
			{6.0, 4.0, 0.03},
			{30.0, 5.0, 0.03},
			{210.0, 7.0, 0.03},
			{2310.0, 11.0, 0.03},
			{30030.0, 13.0, 0.03},
			{510510.0, 17.0, 0.03},
			{9699690.0, 19.0, 0.03},
			{223092870.0, 23.0, 0.03},
			{-2.0, -2.0, 0.03}
		});
	}

	public PrimordialTest(double esperado, double actual, double delta) {
		this.expected = esperado;
		this.actual = actual;
		this.delta = delta;
	}
	
	@Before
	public void crearObj(){
		primordial = new Primordial();
	}
	
	@Test
	public void test(){
		Assert.assertEquals(expected, primordial.get_primordial((int) actual), delta);
	}
}
